import React from 'react';

/**
 *
 * @returns Componente principal de nuestra aplicación
 */
const App = () => {
    return (
      <div>
        <h1>Proyecto final React JS</h1>
      </div>
    );
};

export default App;
